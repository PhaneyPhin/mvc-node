"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Post_1 = require("../entity/Post");
const User_1 = require("../entity/User");
const base_controller_1 = require("./base.controller");
/**
 * @controller PostController
 */
class PostController extends base_controller_1.default {
    constructor() {
        super(...arguments);
        /**
         * Get All Post
         * @param request
         * @param response
         */
        this.getAllPost = (request, response) => __awaiter(this, void 0, void 0, function* () {
            const posts = yield Post_1.Post.find();
            this.responseSuccess(posts, response);
        });
        /**
         * Add Post
         * @param request
         * @param response
         */
        this.addPost = (request, response) => __awaiter(this, void 0, void 0, function* () {
            const { title, text } = request.body;
            const post = Post_1.Post.create({ title, text });
            yield post.save();
            response.send(post);
        });
        /**
         * Update Post
         * @param reqest
         * @param response
         */
        this.updatePost = (request, response) => __awaiter(this, void 0, void 0, function* () {
            const { id } = request.params;
            const { title, text } = request.body;
            const post = yield Post_1.Post.update(id, { title, text });
            return this.responseSuccess(post, response);
        });
        this.getUser = (req, response) => __awaiter(this, void 0, void 0, function* () {
            const users = yield User_1.User.find();
            return response.json(users);
        });
    }
}
exports.default = PostController;
//# sourceMappingURL=post.controller.js.map