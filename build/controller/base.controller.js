"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class BaseController {
    constructor() {
        this.successResponse = { success: true };
        this.failedResponse = { success: false };
        this.responseSuccess = (data, res) => {
            return res.status(200).send(Object.assign(Object.assign({}, this.successResponse), { data }));
        };
        this.responseInterError = (message, res) => {
            return res.status(500).send(Object.assign(Object.assign({}, this.failedResponse), { message }));
        };
        this.responsePageNotFound = (res) => {
            return res.status(404).send(Object.assign(Object.assign({}, this.failedResponse), { message: 'Page Not Found' }));
        };
    }
}
exports.default = BaseController;
//# sourceMappingURL=base.controller.js.map