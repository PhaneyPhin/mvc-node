"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class DateTimeService {
}
exports.default = DateTimeService;
DateTimeService.DATE_FORMAT = 'YYYY-MM-DD';
DateTimeService.DATETIME_FORMAT = 'YYYY-MM-DD HH:mm:ss';
DateTimeService.DATETIME_12HOUR_FORMAT = 'YYYY-MM-DD hh:mm:ss A';
//# sourceMappingURL=date.service.js.map