"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const post_controller_1 = require("../controller/post.controller");
const router = express_1.Router();
const postController = new post_controller_1.default();
router.get('/', postController.getAllPost);
exports.default = router;
//# sourceMappingURL=user.route.js.map