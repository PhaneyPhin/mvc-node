import { Response } from 'express'

export default class BaseController {
    public successResponse = { success: true }
    public failedResponse = { success: false }

    public responseSuccess = (data, res: Response) => {
        return res.status(200).send({ ...this.successResponse, data })
    }

    public responseInterError = (message, res: Response) => {
        return res.status(500).send({ ...this.failedResponse, message })
    }

    public responsePageNotFound = (res: Response) => {
        return res.status(404).send({ ...this.failedResponse, message: 'Page Not Found' })
    }

}