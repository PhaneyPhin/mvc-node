import { Request, Response } from "express"
import { Post } from "../entity/Post"
import { User } from "../entity/User"
import BaseController from "./base.controller"

/**
 * @controller PostController
 */
export default class PostController extends BaseController {
  /**
   * Get All Post
   * @param request
   * @param response
   */
  public getAllPost = async (request: Request, response: Response) => {
    const posts = await Post.find()
    this.responseSuccess(posts, response)
  }

  /**
   * Add Post
   * @param request
   * @param response
   */
  public addPost = async (request: Request, response: Response) => {
    const { title, text } = request.body
    const post = Post.create({ title, text })
    await post.save()
    response.send(post)
  }

  /**
   * Update Post
   * @param reqest
   * @param response
   */

  public updatePost = async (request: Request, response: Response) => {
    const { id } = request.params
    const { title, text } = request.body
    const post = await Post.update(id, { title, text })
    return this.responseSuccess(post, response)
  }

  public getUser = async (req: Request, response: Response) => {
    const users = await User.find()
    return response.json(users)
  }
}
