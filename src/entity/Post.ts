import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable, BaseEntity, BeforeInsert} from "typeorm";
import {Category} from "./Category";

@Entity()
export class Post extends BaseEntity{

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column("text")
    text: string;

    @BeforeInsert()
    Logger = async () => {
        console.log("user", this)
    }

    @ManyToMany(type => Category, {
        cascade: true
    })
    @JoinTable()
    categories: Category[];

}