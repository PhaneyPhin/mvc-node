export default class DateTimeService {
    public static DATE_FORMAT = 'YYYY-MM-DD';
    public static DATETIME_FORMAT = 'YYYY-MM-DD HH:mm:ss';
    public static DATETIME_12HOUR_FORMAT = 'YYYY-MM-DD hh:mm:ss A'

}