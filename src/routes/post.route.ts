import { Router } from "express";
import PostController from "../controller/post.controller";

const router = Router();

const postController = new PostController();

router.get("/", postController.getAllPost);
router.post("/add", postController.addPost);
router.put("/update/:id", postController.updatePost);
router.get("/getUser", postController.getUser);

export default router;
